<?php

namespace Drupal\base64_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "base64_formatter",
 *   label = @Translation("File Base64 encode"),
 *   description = @Translation("File Base64 encode."),
 *   field_types = {
 *     "integer",
 * 
 *   }
 * )
 */
class Base64Formatter extends FormatterBase {
  
  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() === 'file';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
    ) + parent::defaultSettings();
  }
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = array();
    return $element;
  }
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return array();
  }
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $arrFile = File::load($item->value);
      if(!empty($arrFile)){
        $path = $arrFile->getFileUri();
        if(!isset($path) && empty($path)){
          return $elements;
        }
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = base64_encode($data);
        $elements[$delta] = ['#plain_text' => $base64];
      }
    }
    
    return $elements;
  }
  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
  }

}
